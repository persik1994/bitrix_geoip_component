Компонент GeoIP поиска.
Осуществляется с помощью Soap-запроса в публичный веб-сервис http://www.webservicex.net/geoipservice.asmx?WSDL
Я использовал вот этот wsdl, ссылка http://wsgeoip.lavasoft.com/ipservice.asmx?WSDL Его можно найти в файле template.php в строке 33. В данном примере можно поменять wsdl на людой другой, соответственно немного подкорректировав код, но я взял конкретно эту ссылку. Также есть файл components/test/geoip/templates/.default/js/script.js , в котором есть закомментированый код для ajax отправки формы. Но он треует тестирования. В components/test/geoip/templates/.default/css/style.css описаны стили формы данного компонента. 
Результат показывается в таком виде, согласно wsdl: Страна - Штат.
Валидация присутсвует как на уровне сервера, так и клиента. Все это есть в коде.
Уточню, что на сайте обязательно должен работать jQuery. Его можно подключить командой CJSCore::Init(array("jquery"))
Папка test - пространство имен 
папка geoip - сам компонент