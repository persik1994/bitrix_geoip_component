<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
 
$arComponentDescription = array(
	"NAME" => "Геолокация пользователя",
	"DESCRIPTION" => "Выводим местонахождение пользователя",
	"PATH" => array(
		"ID" => "Геолокация пользователя"
	)
);
?>