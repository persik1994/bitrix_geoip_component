<?php

if (check_bitrix_sessid() && (!empty($_REQUEST["submit"]))) {
	ini_set("soap.wsdl_cache_enabled", 1);
	if(isset($_POST['sIp']) && !empty($_POST['sIp'])) 
	{
	    $ip = $_POST['sIp'];
	    if(filter_var($ip, FILTER_VALIDATE_IP)) {
	        try {
	            $opts = array(
	                'http' => array(
	                    'user_agent' => 'PHPSoapClient'
	                )
	            );
	            $context = stream_context_create($opts);

	            $wsdlUrl = 'http://wsgeoip.lavasoft.com/ipservice.asmx?WSDL';
	            $soapClientOptions = array(
	                'stream_context' => $context,
	                'cache_wsdl' => WSDL_CACHE_NONE
	            );

	            $client = new SoapClient($wsdlUrl, $soapClientOptions);
	            $requestParams = array(
	                'sIp' => $ip
	            );

	            $result = $client->GetIpLocation_2_0($requestParams);


	            $location = trim($result->GetIpLocation_2_0Result);
	            $location = substr_replace($location, "-", 10, 0);
	            $location = substr_replace($location, "-",19, 0);
	            echo "<div class='geoip__title'><b>Your Region-State:</b> " . $location . "</div>";

	        }
	        catch(Exception $e) {
	            echo $e->getMessage();
	        }
	    } else {
	        echo "<div class='geoip__title'><b>Невалидный ip адрес</b></div>";
	    }
	} else {
	    echo "<div class='geoip__title'><b>Введите ip адрес!</b></div>";  
	}
}
?>